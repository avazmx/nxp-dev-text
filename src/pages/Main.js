import React from 'react'
import { Navbar } from '../components/Navbar';
import { Header } from '../components/Header';
import { MainPage } from '../pages/MainPage';
import { Footer } from '../components/footer/Footer';

export const Main = () => (
    <div>
        <Navbar/>
        <Header/>
        <MainPage/>
        <Footer/>
    </div>
);

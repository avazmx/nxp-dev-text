import React from 'react'
import '../styles/mainPage.css'
import preview from '../assets/img/preview.jpg'

export const MainPage = () => (
    <div id="mainPage">
        <section className="solutions">
            <h2 className="margin h2">Solutions</h2>
            <div className="margin">
                <div className="solutionsRow">
                    <img src={preview} className="imgPreview" alt="img" />
                    <h4>Title of column</h4>
                    <p>
                        The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                    </p>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                    </div>
                </div>
                <div className="solutionsRow">
                    <img src={preview} className="imgPreview" alt="img"/>
                    <h4>Title of column</h4>
                    <p>
                        Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                    </div>
                </div>
                <div className="solutionsRow">
                    <img src={preview} className="imgPreview" alt="img"/>
                    <h4>Title of column</h4>
                    <p>
                        Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.
                    </p>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                    </div>
                </div>
            </div>
        </section>

        <section className="devRes solutions">
            <h2 className="margin h2">Developer Resources</h2>
            <div className="margin">
                <div className="solutionsRow devResRow">
                    <h4>Title of column</h4>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                    </div>
                </div>
                <div className="solutionsRow devResRow">
                    <h4>Title of column</h4>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                    </div>
                </div>
                <div className="solutionsRow devResRow">
                    <h4>Title of column</h4>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                    </div>
                </div>
            </div>
        </section>

        <section className="solutions">
            <h2 className="margin h2">Products</h2>
            <div className="margin">
                <div className="solutionsRow">
                    <h4>Title of column</h4>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                        <a href="." className="link">Link 4</a>
                        <a href="." className="link">Link 5</a>
                    </div>
                </div>
                <div className="solutionsRow">
                    <h4>Title of column</h4>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                        <a href="." className="link">Link 4</a>
                        <a href="." className="link">Link 5</a>
                    </div>
                </div>
                <div className="solutionsRow">
                    <h4>Title of column</h4>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                        <a href="." className="link">Link 4</a>
                        <a href="." className="link">Link 5</a>
                    </div>
                </div>
                <div className="solutionsRow">
                    <h4>Title of column</h4>
                    <div className="links">
                        <a href="." className="link">Link 1</a>
                        <a href="." className="link">Link 2</a>
                        <a href="." className="link">Link 3</a>
                        <a href="." className="link">Link 4</a>
                        <a href="." className="link">Link 5</a>
                    </div>
                </div>
            </div>
        </section>
    </div>
);
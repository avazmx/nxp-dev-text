import React from 'react'
import '../styles/header.css'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons'

export const Header = () => (
    <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={25}
        totalSlides={3}
    >
        <Slider>
            <Slide index={0} className="slideInfo">
                <h1> I am the first Slide. </h1>
            </Slide>
            <Slide index={1} className="slideInfo">
                <h1> I am the second Slide. </h1>
            </Slide>
            <Slide index={2} className="slideInfo">
                <h1> I am the third Slide. </h1>
            </Slide>
        </Slider>
        <ButtonBack className="back"><FontAwesomeIcon icon={faChevronLeft} /></ButtonBack>
        <ButtonNext className="next"><FontAwesomeIcon icon={faChevronRight} /></ButtonNext>
    </CarouselProvider>
);

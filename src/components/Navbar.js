import React from 'react'
import '../styles/navbar.css'
import logo from '../assets/img/logo.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart, faUser, faGlobe } from '@fortawesome/free-solid-svg-icons'

export const Navbar = () => (
    <div id="navbar">
        <div className="top">
            <div className="left">
                <img src={logo} alt="NXP" />
            </div>
            <div className="right">
                <span className="item">
                    <FontAwesomeIcon icon={faUser} />
                    &nbsp; Username
                </span>
                <span className="item">
                    <FontAwesomeIcon icon={faGlobe} />
                    &nbsp; Language
                </span>
                <span className="item">
                    <FontAwesomeIcon icon={faShoppingCart} />
                    &nbsp; Cart
                </span>
            </div>
        </div>
        <div className="bottom">
            <div className="left">
                <span className="item">Products</span>
                <span className="item">Solutions</span>
                <span className="item">Support</span>
                <span className="item">About</span>
            </div>
            <div className="right">
                <select>
                    <option value="one">Option 1</option>
                    <option value="two">Option 2</option>
                    <option value="three">Option 3</option>
                    <option value="four">Option 4</option>
                </select>
                <input placeholder="Search" />
                <button>Search</button>
            </div>
        </div>
    </div>
);

import React from 'react'
import '../../styles/footer.css'
import 'pure-react-carousel/dist/react-carousel.es.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'
import FooterCarousel from './FooterCarousel'

export const Footer = () => (
    <footer id="footer">
        <div className="margin">
            <div className="solutionsRow">
                <h4 className="h4Footer">About NXP</h4>
                <div className="links">
                    <a href="." className="link">Link 1</a>
                    <a href="." className="link">Link 2</a>
                    <a href="." className="link">Link 3</a>
                </div>
            </div>
            <div className="solutionsRow">
                <h4 className="h4Footer">Resources</h4>
                <div className="links">
                    <a href="." className="link">Link 1</a>
                    <a href="." className="link">Link 2</a>
                </div>
            </div>
            <div className="solutionsRow">
                <h4 className="h4Footer">Follow us</h4>
                <div>
                    <FontAwesomeIcon className="icon" icon={faEnvelope} />
                    <FontAwesomeIcon className="icon" icon={faEnvelope} />
                    <FontAwesomeIcon className="icon" icon={faEnvelope} />
                    <FontAwesomeIcon className="icon" icon={faEnvelope} />
                </div>
            </div>
            <div className="solutionsRow">
                <FooterCarousel/>
            </div>
        </div>

        <div className="bottomFooter">
            <div className="bottomFooterInfo">
                <div className="bottomFooterLeft">
                    <span className="bottomFooteritem">Privacy</span> &nbsp; | &nbsp;
                    <span className="bottomFooteritem">Terms of Use</span> &nbsp; | &nbsp;
                    <span className="bottomFooteritem">Terms of Sale</span> &nbsp; | &nbsp;
                    <span className="bottomFooteritem">Feedback</span>
                </div>
                <div className="bottomFooterRight">
                    &copy;2006-2018 NXP Semiconductors. All rights reserved.
                </div>
            </div>
        </div>
    </footer>
);

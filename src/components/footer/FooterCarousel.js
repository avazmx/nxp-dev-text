import React, { Component } from 'react'
import '../../styles/footerCarousel.css'
import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons'

export default class FooterCarousel extends Component {
    constructor() {
        super();
        this.state = {
            error: null,
            isLoading: false,
            data: [],
        }
    }

    componentDidMount() {
        this.setState({ isLoading: true });
        fetch('https://newsapi.org/v2/top-headlines?sources=bbc-news&apiKey=5f38b34ab2c44a6d84d3ed1075fcafa8')
            .then(res => res.json())
            .then((response) => {
                console.log(response.articles);
                this.setState({
                    isLoaded: true,
                    data: response.articles,
                })
            },

            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                })
            }
        )
    }

    render () {
        const { error, isLoading } = this.state;
        if (error) {
            return <div>Error: {error.message} </div>
        } else if (!isLoading) {
            return <div>Loading...</div>;
        } else {
            return (
                <CarouselProvider
                    naturalSlideWidth={100}
                    naturalSlideHeight={80}
                    totalSlides={this.state.data.length}
                    id="footerCarousel"
                >
                    <Slider>
                        {this.state.data.map(
                            (dynamicData, author, publishedAt, title, description, url, index) =>        
                            <Slide index={dynamicData} key={dynamicData.url} className="slideInfoFooter">
                                <h4> {dynamicData.author}: &nbsp; {dynamicData.publishedAt.substr(0,10)}
                                </h4>
                                <h5> {dynamicData.title} </h5>
                                <p className="infoP"> {dynamicData.description} </p>
                                <a className="link" href={dynamicData.url}> Read More</a>
                            </Slide>
                        )}
                    </Slider>
                    <ButtonBack className="backFooter"><FontAwesomeIcon icon={faChevronLeft} /></ButtonBack>
                    <ButtonNext className="nextFooter"><FontAwesomeIcon icon={faChevronRight} /></ButtonNext>
                </CarouselProvider>
            )
        }
    }
}

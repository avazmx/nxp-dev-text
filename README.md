## NXP Dev Test

1. Clone / Download.
2. `npm install`.
3. enjoy.

- [Mockup](#mockup)
- [Carousel](#carousel)
- [Challenges](#challenges)

## Updating to New Releases

The challenge was: 
- 1. Mockup of the nxp.com homepage, 
- 2. Create a carousel in the footer which fetch the data from https://newsapi.org/

## Mockup
I decided to go for 4 sections: `Navbar`, `Header`, `MainPage` and `Footer`.
1) For the Navbar and Footer, it was almost the same thing: An element divided in 2 sections (top and bottom).
2) For the header it was basically the full width carousel.
3) Finally, for the MainPage was an element divided by 3 sections: Solutions, Developer Resources and Products. Those 3 were basically almost the same, since they were divided in 3 rows each, except for the last one, which has 4 rows. And also to mention that the first column had an image in each row.

## Carousel
For the carousel is was just fetching the API from https://newsapi.org/.
For this one i used a plugin called `pure-react-carousel`, which basically bring me all the elements for me to be able to set a carousel.

## Challenges
1) One of the challenges that i faced was that this API didn't had an `id` for their elements (which i needed, since when you map data, you need to set a key value). The problem was resolved by  just using `index` instead.
2) Another problem was the time format, for some reason `tolocaleDateString()` didn't worked. As a work around, i set `substr(0,10)`.